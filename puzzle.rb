require_relative 'cell'
require_relative 'sections'
require 'matrix'

class Puzzle
	def initialize(file)
		@matrix = Matrix.build(9,9) { Cell.new }
		@sections = getSections

		row,column = 0,0
		# Populate initial values
		File.foreach(file) do |line|
			line.chomp.split(' ').each do |num|
				unless num == '0'
					setValue(row,column,num)
				end
				column += 1
			end
			row += 1
			column = 0
		end
	end

	def printPuzzle
		puts "Puzzle:"
		9.times do |row|
			puts @matrix.row(row).to_a
		end
	end

	def solve
		not_solid = true
		while not_solid
			not_solid = logify
		end
		puts "Fully logified"
	end

	def logify
		somethingChanged = false
		9.times do |row|
			9.times do |column|
				puts "Doing #{row},#{column} -> #{@matrix[row,column].value}: #{@matrix[row,column].possibles}"
				if @matrix[row,column].possibles.count == 1
					value = @matrix[row,column].possibles[0]
					puts "  Only one possible: #{value}"
					setValue(row,column,value)
					somethingChanged = true
					puts "#{somethingChanged}"
				end
			end
		end
		somethingChanged
	end

	def isSolved?
		9.times do |row|
			print "row: #{@matrix.row(row)}\nunique: #{@matrix.row(row).to_a.uniq}\n"
			puts "#{@matrix.row(row)[0].class}"
			if @matrix.row(row).to_a != @matrix.row(row).to_a.uniq then return false end
		end
		9.times do |column|
			if @matrix.column(column).to_a != @matrix.column(column).to_a.uniq then return false end
		end
		return true
	end

	# Finds a cell with min number of possibles, takes a guess,
	# and then attempts to solve, returns true if puzzle solves
	def guessAndGo
		# consider: min number of possibles w/in a cell, and least occuring possible number
	end

	# Sets the value at row and column to value,
	# then clears row, column, and section of possibles
	def setValue(row,column,value)
		puts "  Setting #{row},#{column} to #{value}"
		@matrix[row,column].value = value
		@matrix[row,column].possibles = []

		@matrix.row(row).each { |cell| cell.possibles.delete(value.to_i) }
		@matrix.column(column).each { |cell| cell.possibles.delete(value.to_i) }
		clearSection(value,@sections[row,column])
	end
	def clearSection(num, section)
		9.times do |row|
			9.times do |column|
				if @sections[row,column] == section
					@matrix[row,column].possibles.delete(num.to_i)
				end
			end
		end
	end
end
