#!/usr/bin/env ruby

require_relative 'puzzle'

puts 'Starting Program...'

if ARGV.size != 1
	puts 'USAGE: ruby solver.rb <file>'
	exit
end

puzzle = Puzzle.new(ARGV[0])
puzzle.printPuzzle

puzzle.solve

puts "Is solved:  #{puzzle.isSolved?}"

puzzle.printPuzzle

puts '...Program Ended'
