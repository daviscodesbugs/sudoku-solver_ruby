class Cell
	def initialize
		@value = 0
		@possibles = Array (1..9)
	end

	def inspect; "#{@value}" end
	def to_s; "#{@value}" end

	attr_accessor :value, :possibles

	def hasValue?
		unless @value == 0 then return true end
		false
	end
end
