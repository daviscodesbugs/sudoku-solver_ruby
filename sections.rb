require 'matrix'

def getSections
	Matrix.rows([
		['A','A','A','B','B','B','C','C','C'],
		['A','A','A','B','B','B','C','C','C'],
		['A','A','A','B','B','B','C','C','C'],
		['D','D','D','E','E','E','F','F','F'],
		['D','D','D','E','E','E','F','F','F'],
		['D','D','D','E','E','E','F','F','F'],
		['G','G','G','H','H','H','I','I','I'],
		['G','G','G','H','H','H','I','I','I'],
		['G','G','G','H','H','H','I','I','I']
	])
end
